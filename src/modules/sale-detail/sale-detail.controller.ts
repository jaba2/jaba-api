import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseIntPipe,
  Post,
  UseFilters,
} from '@nestjs/common';
import { EntityNotFoundErrorFilter } from 'src/common/exceptions/entity-not-found-error.filter';
import { UpdateResult } from 'typeorm';
import { SaleDetailDTO } from './sale-detail.dto';
import { SaleDetail } from './sale-detail.entity';
import { SaleDetailService } from './sale-detail.service';

@Controller('sale-detail')
export class SaleDetailController {
  constructor(private saleDetailService: SaleDetailService) {}

  @Get('/all')
  public findAll(): Promise<SaleDetail[]> {
    return this.saleDetailService.findAll();
  }

  @Get('/:id')
  @UseFilters(EntityNotFoundErrorFilter)
  public findOne(
    @Param('id', ParseIntPipe) saleDetailId: number,
  ): Promise<SaleDetail> {
    return this.saleDetailService.findOne(saleDetailId);
  }

  @Delete('/:id')
  public softDelete(
    @Param('id', ParseIntPipe) saleDetailId: number,
  ): Promise<UpdateResult> {
    return this.saleDetailService.softDelete(saleDetailId);
  }

  @Post('/')
  public save(@Body() saleDetail: SaleDetailDTO): Promise<SaleDetail> {
    return this.saleDetailService.save(saleDetail);
  }
}
