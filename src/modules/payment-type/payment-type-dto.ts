import { IsNotEmpty, IsString } from 'class-validator';

export class PaymentTypeDTO {
  @IsString()
  @IsNotEmpty()
  name: string;

  @IsString()
  description: string;
}
