import { ConfigService } from '@nestjs/config';
import { ConfigKeys } from '../config/config.keys';
import { DataSource, DataSourceOptions } from 'typeorm';

export const databaseProviders = [
  {
    provide: 'DATA_SOURCE',
    useFactory: async () => {
      const configService = new ConfigService();
      const postgresConfig: DataSourceOptions = {
        type: 'postgres',
        host: configService.get<string>(ConfigKeys.DB_HOST),
        port: configService.get<number>(ConfigKeys.DB_PORT),
        username: configService.get<string>(ConfigKeys.DB_USER),
        password: configService.get<string>(ConfigKeys.DB_PASSWORD),
        database: configService.get<string>(ConfigKeys.DB_NAME),
        entities: [__dirname + '/../**/*.entity{.ts,.js}'],
        migrations: [__dirname + '/migration/*{.ts,.js}'],
        synchronize: false,
      };
      const dataSource = new DataSource(postgresConfig);
      return dataSource.initialize();
    },
  },
];
