import { MigrationInterface, QueryRunner } from "typeorm";

export class InitialDB1659137179045 implements MigrationInterface {
    name = 'InitialDB1659137179045'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "jaba"."product_types" ("product_type_id" SERIAL NOT NULL, "product_type_name" character varying(50) NOT NULL, "product_type_code" character varying(50) NOT NULL, "product_type_description" character varying(255), "product_type_created_date" TIMESTAMP NOT NULL DEFAULT now(), "product_type_updated_date" TIMESTAMP NOT NULL DEFAULT now(), "product_type_deleted_date" TIMESTAMP, CONSTRAINT "PK_91a2058eff2209e67033c7378dd" PRIMARY KEY ("product_type_id"))`);
        await queryRunner.query(`CREATE TABLE "jaba"."products" ("product_id" SERIAL NOT NULL, "product_name" character varying(50) NOT NULL, "product_code" character varying(50) NOT NULL, "product_description" character varying(255), "product_initial_quantity" bigint NOT NULL, "product_quantity" bigint NOT NULL, "product_created_date" TIMESTAMP NOT NULL DEFAULT now(), "product_updated_date" TIMESTAMP NOT NULL DEFAULT now(), "product_deleted_date" TIMESTAMP, "product_product_type_id" integer, "product_business_id" integer, CONSTRAINT "PK_a8940a4bf3b90bd7ac15c8f4dd9" PRIMARY KEY ("product_id"))`);
        await queryRunner.query(`CREATE TABLE "jaba"."sale_details" ("sale_details_id" SERIAL NOT NULL, "sale_details_description" character varying(255), "sale_details_quantity" bigint NOT NULL, "sale_details_amount" bigint NOT NULL, "sale_details_total_amount" bigint NOT NULL, "sale_details_created_date" TIMESTAMP NOT NULL DEFAULT now(), "sale_details_updated_date" TIMESTAMP NOT NULL DEFAULT now(), "sale_details_deleted_date" TIMESTAMP, "sale_details_sale_id" integer, "sale_details_product_id" integer, CONSTRAINT "PK_9bbb8923abeb8ee3c3dc0187a59" PRIMARY KEY ("sale_details_id"))`);
        await queryRunner.query(`CREATE TABLE "jaba"."sales" ("sales_id" SERIAL NOT NULL, "sales_description" character varying(255), "sales_client_name" character varying(55) NOT NULL, "sales_quantity" bigint NOT NULL, "sales_amount" bigint NOT NULL, "sales_created_date" TIMESTAMP NOT NULL DEFAULT now(), "sales_updated_date" TIMESTAMP NOT NULL DEFAULT now(), "sales_deleted_date" TIMESTAMP, "paymentTypeId" integer, "sales_business_id" integer, CONSTRAINT "PK_c4b190155c1d24ef7a087e5a39a" PRIMARY KEY ("sales_id"))`);
        await queryRunner.query(`CREATE TABLE "jaba"."payment_types" ("payment_types_id" SERIAL NOT NULL, "payment_types_name" character varying(50) NOT NULL, "payment_types_description" character varying(255), "payment_types_created_date" TIMESTAMP NOT NULL DEFAULT now(), "payment_types_updated_date" TIMESTAMP NOT NULL DEFAULT now(), "payment_types_deleted_date" TIMESTAMP, CONSTRAINT "PK_dc022dbe71c95efaf5935df074b" PRIMARY KEY ("payment_types_id"))`);
        await queryRunner.query(`CREATE TABLE "jaba"."investments" ("investments_id" SERIAL NOT NULL, "investments_name" character varying(50) NOT NULL, "investments_description" character varying(255), "investments_amount" bigint NOT NULL, "investments_created_date" TIMESTAMP NOT NULL DEFAULT now(), "investments_updated_date" TIMESTAMP NOT NULL DEFAULT now(), "investments_deleted_date" TIMESTAMP, "paymentTypeId" integer, "investments_business_id" integer, CONSTRAINT "PK_6b1cb92147c9ab62a0cf3a9d5b9" PRIMARY KEY ("investments_id"))`);
        await queryRunner.query(`CREATE TABLE "jaba"."vendor" ("vendor_id" SERIAL NOT NULL, "vendor_name" character varying(50) NOT NULL, "vendor_description" character varying(255), "vendor_created_date" TIMESTAMP NOT NULL DEFAULT now(), "vendor_updated_date" TIMESTAMP NOT NULL DEFAULT now(), "vendor_deleted_date" TIMESTAMP, CONSTRAINT "PK_f82665d231fd62808e6953014b2" PRIMARY KEY ("vendor_id"))`);
        await queryRunner.query(`CREATE TABLE "jaba"."business" ("business_id" SERIAL NOT NULL, "business_name" character varying(50) NOT NULL, "business_description" character varying(255), "business_investment_amount" bigint NOT NULL, "business_created_date" TIMESTAMP NOT NULL DEFAULT now(), "business_updated_date" TIMESTAMP NOT NULL DEFAULT now(), "business_deleted_date" TIMESTAMP, "business_vendor_id" integer, CONSTRAINT "PK_a8b2281570e69c768f3c363184b" PRIMARY KEY ("business_id"))`);
        await queryRunner.query(`ALTER TABLE "jaba"."products" ADD CONSTRAINT "FK_15c89d223683fecd49907c6d738" FOREIGN KEY ("product_product_type_id") REFERENCES "jaba"."product_types"("product_type_id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "jaba"."products" ADD CONSTRAINT "FK_614de8c4a378e0abb936715f840" FOREIGN KEY ("product_business_id") REFERENCES "jaba"."business"("business_id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "jaba"."sale_details" ADD CONSTRAINT "FK_e3d4c157797f3e356c2f30c8322" FOREIGN KEY ("sale_details_sale_id") REFERENCES "jaba"."sales"("sales_id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "jaba"."sale_details" ADD CONSTRAINT "FK_a10bace25cf71ab72453be3e659" FOREIGN KEY ("sale_details_product_id") REFERENCES "jaba"."products"("product_id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "jaba"."sales" ADD CONSTRAINT "FK_efe4e7ea1c9f247fd64b333ad58" FOREIGN KEY ("paymentTypeId") REFERENCES "jaba"."payment_types"("payment_types_id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "jaba"."sales" ADD CONSTRAINT "FK_5c88073ce3fa3e696d3c9531992" FOREIGN KEY ("sales_business_id") REFERENCES "jaba"."business"("business_id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "jaba"."investments" ADD CONSTRAINT "FK_d172b7d7e0b8c129f407cd5fb1e" FOREIGN KEY ("paymentTypeId") REFERENCES "jaba"."payment_types"("payment_types_id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "jaba"."investments" ADD CONSTRAINT "FK_5026c8f11f5faec5ac371dcde53" FOREIGN KEY ("investments_business_id") REFERENCES "jaba"."business"("business_id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "jaba"."business" ADD CONSTRAINT "FK_1bc597001b6d3235922cb2c3fd5" FOREIGN KEY ("business_vendor_id") REFERENCES "jaba"."vendor"("vendor_id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "jaba"."business" DROP CONSTRAINT "FK_1bc597001b6d3235922cb2c3fd5"`);
        await queryRunner.query(`ALTER TABLE "jaba"."investments" DROP CONSTRAINT "FK_5026c8f11f5faec5ac371dcde53"`);
        await queryRunner.query(`ALTER TABLE "jaba"."investments" DROP CONSTRAINT "FK_d172b7d7e0b8c129f407cd5fb1e"`);
        await queryRunner.query(`ALTER TABLE "jaba"."sales" DROP CONSTRAINT "FK_5c88073ce3fa3e696d3c9531992"`);
        await queryRunner.query(`ALTER TABLE "jaba"."sales" DROP CONSTRAINT "FK_efe4e7ea1c9f247fd64b333ad58"`);
        await queryRunner.query(`ALTER TABLE "jaba"."sale_details" DROP CONSTRAINT "FK_a10bace25cf71ab72453be3e659"`);
        await queryRunner.query(`ALTER TABLE "jaba"."sale_details" DROP CONSTRAINT "FK_e3d4c157797f3e356c2f30c8322"`);
        await queryRunner.query(`ALTER TABLE "jaba"."products" DROP CONSTRAINT "FK_614de8c4a378e0abb936715f840"`);
        await queryRunner.query(`ALTER TABLE "jaba"."products" DROP CONSTRAINT "FK_15c89d223683fecd49907c6d738"`);
        await queryRunner.query(`DROP TABLE "jaba"."business"`);
        await queryRunner.query(`DROP TABLE "jaba"."vendor"`);
        await queryRunner.query(`DROP TABLE "jaba"."investments"`);
        await queryRunner.query(`DROP TABLE "jaba"."payment_types"`);
        await queryRunner.query(`DROP TABLE "jaba"."sales"`);
        await queryRunner.query(`DROP TABLE "jaba"."sale_details"`);
        await queryRunner.query(`DROP TABLE "jaba"."products"`);
        await queryRunner.query(`DROP TABLE "jaba"."product_types"`);
    }

}
