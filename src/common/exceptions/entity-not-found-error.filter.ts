import {
  ArgumentsHost,
  Catch,
  ExceptionFilter,
  HttpStatus,
} from '@nestjs/common';
import { Response, Request } from 'express';
import { EntityNotFoundError } from 'typeorm';
import { EXCEPTIONS_MESSAGE } from './constants/exceptions.constants';

@Catch(EntityNotFoundError)
export class EntityNotFoundErrorFilter implements ExceptionFilter {
  catch(exception: EntityNotFoundError, host: ArgumentsHost) {
    const context = host.switchToHttp();
    const response = context.getResponse<Response>();
    const request = context.getRequest<Request>();
    const statusCode = HttpStatus.NOT_FOUND;
    response.status(statusCode).json({
      statusCode,
      message: EXCEPTIONS_MESSAGE.RESOURCE_NOT_FOUND,
      payload: {
        id: request.params.id
          ? request.params.id
          : EXCEPTIONS_MESSAGE.ID_NOT_FOUND,
      },
    });
  }
}
