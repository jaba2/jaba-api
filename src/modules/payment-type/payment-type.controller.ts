import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseIntPipe,
  Post,
  UseFilters,
} from '@nestjs/common';
import { EntityNotFoundErrorFilter } from 'src/common/exceptions/entity-not-found-error.filter';
import { UpdateResult } from 'typeorm';
import { PaymentTypeDTO } from './payment-type-dto';
import { PaymentType } from './payment-type.entity';
import { PaymentTypeService } from './payment-type.service';

@Controller('payment-type')
export class PaymentTypeController {
  constructor(private paymentTypeService: PaymentTypeService) {}

  @Get('/all')
  public findAll(): Promise<PaymentType[]> {
    return this.paymentTypeService.findAll();
  }

  @Get('/:id')
  @UseFilters(EntityNotFoundErrorFilter)
  public findOne(
    @Param('id', ParseIntPipe) paymentType: number,
  ): Promise<PaymentType> {
    return this.paymentTypeService.findOne(paymentType);
  }

  @Delete('/:id')
  public softDelete(
    @Param('id', ParseIntPipe) paymentType: number,
  ): Promise<UpdateResult> {
    return this.paymentTypeService.softDelete(paymentType);
  }

  @Post('/')
  public save(@Body() vendor: PaymentTypeDTO): Promise<PaymentType> {
    return this.paymentTypeService.save(vendor);
  }
}
