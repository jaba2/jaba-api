export enum ConfigKeys {
  DB_HOST = 'DB_HOST',
  DB_LOGGING = 'DB_LOGGING',
  DB_NAME = 'DB_NAME',
  DB_PASSWORD = 'DB_PASSWORD',
  DB_PORT = 'DB_PORT',
  DB_TYPE = 'DB_TYPE',
  DB_USER = 'DB_USER',
  PORT = 'PORT',
}
