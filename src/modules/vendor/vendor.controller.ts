import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseIntPipe,
  Post,
  UseFilters,
} from '@nestjs/common';
import { EntityNotFoundErrorFilter } from '../../common/exceptions/entity-not-found-error.filter';
import { UpdateResult } from 'typeorm';
import { VendorDTO } from './vendor.dto';
import { Vendor } from './vendor.entity';
import { VendorService } from './vendor.service';

@Controller('vendor')
export class VendorController {
  constructor(private vendorService: VendorService) {}

  @Get('/all')
  public findAll(): Promise<Vendor[]> {
    return this.vendorService.findAll();
  }

  @Get('/:id')
  @UseFilters(EntityNotFoundErrorFilter)
  public findOne(@Param('id', ParseIntPipe) vendorId: number): Promise<Vendor> {
    return this.vendorService.findOne(vendorId);
  }

  @Delete('/:id')
  public softDelete(
    @Param('id', ParseIntPipe) vendorId: number,
  ): Promise<UpdateResult> {
    return this.vendorService.softDelete(vendorId);
  }

  @Post('/')
  public save(@Body() vendor: VendorDTO): Promise<Vendor> {
    return this.vendorService.save(vendor);
  }
}
