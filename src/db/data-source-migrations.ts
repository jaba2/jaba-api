import { DataSource, DataSourceOptions } from 'typeorm';
import * as dotenv from 'dotenv';
import * as fs from 'fs';

const config = dotenv.parse(fs.readFileSync(__dirname + '/../../.env'));

const postgresConfig: DataSourceOptions = {
  type: 'postgres',
  host: config.DB_HOST,
  port: parseInt(config.DB_PORT, 10),
  username: config.DB_USER,
  password: config.DB_PASSWORD,
  database: config.DB_NAME,
  entities: [__dirname + '/../**/*.entity{.ts,.js}'],
  migrations: [__dirname + '/migration/*{.ts,.js}'],
  synchronize: false,
};

export const dataSource = new DataSource(postgresConfig);
