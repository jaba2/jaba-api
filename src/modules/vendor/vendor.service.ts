import { Repository, UpdateResult } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Vendor } from './vendor.entity';
import { VendorDTO } from './vendor.dto';
import { Injectable } from '@nestjs/common';

@Injectable()
export class VendorService {
  constructor(
    @InjectRepository(Vendor)
    private vendorRepository: Repository<Vendor>,
  ) {}

  /**
   * @description Finds all active vendor
   * @returns {Promise<Vendor[]>} A list with all active vendor
   */
  public async findAll(): Promise<Vendor[]> {
    return this.vendorRepository.find();
  }

  /**
   * @description Finds one vendor by id
   * @param {number} vendorId
   * @returns {Promise<Vendor}> The found vendor, if not return an exception
   */
  public async findOne(vendorId: number): Promise<Vendor> {
    return this.vendorRepository.findOneByOrFail({ id: vendorId });
  }

  /**
   * @description Applies an soft delete to record
   * @param {number} vendorId
   * @returns {Promise<UpdateResult}
   */
  public async softDelete(vendorId: number): Promise<UpdateResult> {
    return this.vendorRepository.softDelete({ id: vendorId });
  }

  /**
   * @description Saves a vendor record
   * @param {VendorDTO} vendor
   * @returns {Vendor} The vendor saved
   */
  public async save(vendor: VendorDTO): Promise<Vendor> {
    return this.vendorRepository.save(vendor);
  }
}
