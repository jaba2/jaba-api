import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseIntPipe,
  Post,
  UseFilters,
} from '@nestjs/common';
import { EntityNotFoundErrorFilter } from 'src/common/exceptions/entity-not-found-error.filter';
import { UpdateResult } from 'typeorm';
import { ProductTypeDTO } from './product-type.dto';
import { ProductType } from './product-type.entity';
import { ProductTypeService } from './product-type.service';

@Controller('product-type')
export class ProductTypeController {
  constructor(private paymentTypeService: ProductTypeService) {}

  @Get('/all')
  public findAll(): Promise<ProductType[]> {
    return this.paymentTypeService.findAll();
  }

  @Get('/:id')
  @UseFilters(EntityNotFoundErrorFilter)
  public findOne(
    @Param('id', ParseIntPipe) paymentType: number,
  ): Promise<ProductType> {
    return this.paymentTypeService.findOne(paymentType);
  }

  @Delete('/:id')
  public softDelete(
    @Param('id', ParseIntPipe) paymentType: number,
  ): Promise<UpdateResult> {
    return this.paymentTypeService.softDelete(paymentType);
  }

  @Post('/')
  public save(@Body() vendor: ProductTypeDTO): Promise<ProductType> {
    return this.paymentTypeService.save(vendor);
  }
}
