import { HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { BaseException } from 'src/common/exceptions/base.exception';
import { Repository, UpdateResult } from 'typeorm';
import { ProductDTO } from './product.dto';
import { Product } from './product.entity';
import { ProductFactory } from './product.factory';

@Injectable()
export class ProductService {
  constructor(
    @InjectRepository(Product)
    private productRepository: Repository<Product>,
  ) {}

  /**
   * @description Finds all active product
   * @returns {Promise<Product[]>} A list with all active product
   */
  public async findAll(): Promise<Product[]> {
    return this.productRepository.find();
  }

  /**
   * @description Finds one product by id
   * @param {number} productId
   * @returns {Promise<Product}> The found product, if not return an exception
   */
  public async findOne(productId: number): Promise<Product> {
    return this.productRepository.findOneByOrFail({ id: productId });
  }

  /**
   * @description Applies an soft delete to record
   * @param {number} productId
   * @returns {Promise<UpdateResult}
   */
  public async softDelete(productId: number): Promise<UpdateResult> {
    return this.productRepository.softDelete({ id: productId });
  }

  /**
   * @description Saves a product record
   * @param {ProductDTO} product
   * @returns {Product} The product saved
   */
  public async save(product: ProductDTO): Promise<Product> {
    return this.productRepository.save(product);
  }

  public updateQuantityAfterSale(
    product: ProductDTO,
    quantity: number,
  ): Product {
    const productQuantityAfterSale = product.quantity - quantity;
    if (productQuantityAfterSale < 0) {
      throw new BaseException(
        'QUANTITY_IS_LESS_THAN_0',
        HttpStatus.BAD_REQUEST,
        'QUANTITY_IS_LESS_THAN_0',
      );
    }
    product.quantity = productQuantityAfterSale;
    return ProductFactory.buildFromDTO(product);
  }
}
