import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, UpdateResult } from 'typeorm';
import { InvestmentDTO } from './investment.dto';
import { Investment } from './investment.entity';

@Injectable()
export class InvestmentService {
  constructor(
    @InjectRepository(Investment)
    private investmentRepository: Repository<Investment>,
  ) {}

  /**
   * @description Finds all active investment
   * @returns {Promise<Investment[]>} A list with all active investment
   */
  public async findAll(): Promise<Investment[]> {
    return this.investmentRepository.find();
  }

  /**
   * @description Finds one investment by id
   * @param {number} investmentId
   * @returns {Promise<Investment}> The found investment
   */
  public async findOne(investmentId: number): Promise<Investment> {
    return this.investmentRepository.findOneByOrFail({ id: investmentId });
  }

  /**
   * @description Applies an soft delete to record
   * @param {number} investmentId
   * @returns {Promise<UpdateResult}
   */
  public async softDelete(investmentId: number): Promise<UpdateResult> {
    return this.investmentRepository.softDelete({ id: investmentId });
  }

  /**
   * @description Saves a investment record
   * @param {InvestmentDTO} investment
   * @returns {Investment} The investment saved
   */
  public async save(investment: InvestmentDTO): Promise<Investment> {
    return this.investmentRepository.save(investment);
  }
}
