import { ProductDTO } from './product.dto';
import { Product } from './product.entity';

export class ProductFactory {
  /**
   * @description
   * @param productDTO
   * @returns
   */
  public static buildFromDTO(productDTO: ProductDTO): Product {
    const product = new Product();
    product.id = productDTO.id;
    product.name = productDTO.name;
    product.code = productDTO.code;
    product.description = productDTO.description;
    product.initialQuantity = productDTO.initialQuantity;
    product.quantity = productDTO.quantity;
    return product;
  }
}
