import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Investment } from '../investment/investment.entity';
import { Sale } from '../sale/sale.entity';

@Entity('payment_types', { schema: 'jaba' })
export class PaymentType {
  @PrimaryGeneratedColumn({ name: 'payment_types_id' })
  id: number;

  @Column({
    type: 'character varying',
    length: '50',
    name: 'payment_types_name',
    nullable: false,
  })
  name: string;

  @Column({
    type: 'character varying',
    name: 'payment_types_description',
    length: '255',
    nullable: true,
  })
  description: string;

  @OneToMany(() => Investment, (investment) => investment.paymentType)
  investment: Investment;

  @OneToMany(() => Sale, (sale) => sale.paymentType)
  sale: Sale;

  @CreateDateColumn({ name: 'payment_types_created_date' })
  created: Date;

  @UpdateDateColumn({ name: 'payment_types_updated_date' })
  updated: Date;

  @DeleteDateColumn({ name: 'payment_types_deleted_date' })
  deleted: Date;
}
