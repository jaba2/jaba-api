import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, UpdateResult } from 'typeorm';
import { InvestmentDTO } from '../investment/investment.dto';
import { Investment } from '../investment/investment.entity';

import { Product } from '../product/product.entity';
import { Vendor } from '../vendor/vendor.entity';
import { BusinessDTO } from './business.dto';
import { Business } from './business.entity';

@Injectable()
export class BusinessService {
  constructor(
    @InjectRepository(Business)
    private businessRepository: Repository<Business>,
  ) {}

  /**
   * @description Finds all active business
   * @returns {Promise<Business[]>} A list with all active business
   */
  public async findAll(): Promise<Business[]> {
    return this.businessRepository.find();
  }

  /**
   * @description Finds one business by id
   * @param {number} businessId
   * @returns {Promise<Business}> The found business, if not return an exception
   */
  public async findOne(businessId: number): Promise<Business> {
    return this.businessRepository.findOneByOrFail({ id: businessId });
  }

  /**
   * @description Applies an soft delete to record
   * @param {number} businessId
   * @returns {Promise<UpdateResult}
   */
  public async softDelete(businessId: number): Promise<UpdateResult> {
    return this.businessRepository.softDelete({ id: businessId });
  }

  /**
   * @description Saves a business record with products and investments
   * @param {Business} business
   * @returns {Business} The business saved
   */
  public async save(business: BusinessDTO): Promise<Business> {
    const newBusiness: Business = new Business();
    newBusiness.name = business.name;
    newBusiness.description = business.description;
    newBusiness.vendor = business.vendor as Vendor;
    newBusiness.investments = business.investments as Investment[];
    newBusiness.products = business.products as Product[];
    newBusiness.investmentAmount = business.investments.reduce(
      (previousValue: number, investment: InvestmentDTO) =>
        previousValue + investment.amount,
      0,
    );

    return this.businessRepository.save(newBusiness);
  }
}
