import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseIntPipe,
  Post,
  UseFilters,
} from '@nestjs/common';
import { EntityNotFoundErrorFilter } from 'src/common/exceptions/entity-not-found-error.filter';
import { UpdateResult } from 'typeorm';
import { BusinessDTO } from './business.dto';
import { Business } from './business.entity';
import { BusinessService } from './business.service';

@Controller('business')
export class BusinessController {
  constructor(private businessService: BusinessService) {}

  @Get('/all')
  public findAll(): Promise<Business[]> {
    return this.businessService.findAll();
  }

  @Get('/:id')
  @UseFilters(EntityNotFoundErrorFilter)
  public findOne(
    @Param('id', ParseIntPipe) businessId: number,
  ): Promise<Business> {
    return this.businessService.findOne(businessId);
  }

  @Delete('/:id')
  public softDelete(
    @Param('id', ParseIntPipe) businessId: number,
  ): Promise<UpdateResult> {
    return this.businessService.softDelete(businessId);
  }

  @Post('/')
  public save(@Body() business: BusinessDTO): Promise<Business> {
    return this.businessService.save(business);
  }
}
