import { HttpException, Logger } from '@nestjs/common';

export class BaseException extends HttpException {
  public name: string;

  constructor(name: string, status: number, message: string) {
    super(message, status);
    this.name = name;
    Logger.error(message);
  }
}
