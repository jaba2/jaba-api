import { Type } from 'class-transformer';
import {
  IsNotEmpty,
  IsNumber,
  IsObject,
  IsOptional,
  IsString,
  ValidateNested,
} from 'class-validator';
import { ProductTypeDTO } from '../product-type/product-type.dto';

export class ProductDTO {
  @IsOptional()
  id: number;

  @IsString()
  @IsNotEmpty()
  name: string;

  @IsString()
  @IsNotEmpty()
  code: string;

  @IsString()
  description: string;

  @IsNumber()
  @IsNotEmpty()
  initialQuantity: number;

  @IsNumber()
  @IsNotEmpty()
  quantity: number;

  @IsObject()
  @ValidateNested()
  @Type(() => ProductTypeDTO)
  productType: ProductTypeDTO;
}
