import { IsNotEmpty, IsString } from 'class-validator';

export class ProductTypeDTO {
  @IsString()
  @IsNotEmpty()
  name: string;

  @IsString()
  description: string;

  @IsString()
  @IsNotEmpty()
  code: string;
}
