import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Business } from '../business/business.entity';

import { PaymentType } from '../payment-type/payment-type.entity';
import { SaleDetail } from '../sale-detail/sale-detail.entity';

@Entity('sales', { schema: 'jaba' })
export class Sale {
  @PrimaryGeneratedColumn({ name: 'sales_id' })
  id: number;

  @Column({
    type: 'character varying',
    name: 'sales_description',
    length: '255',
    nullable: true,
  })
  description: string;

  @Column({
    type: 'character varying',
    name: 'sales_client_name',
    length: '55',
    nullable: false,
  })
  clientName: string;

  @Column({
    type: 'bigint',
    name: 'sales_quantity',
    nullable: false,
  })
  quantity: number;

  @Column({
    type: 'bigint',
    name: 'sales_amount',
    nullable: false,
  })
  amount: number;

  @ManyToOne(() => PaymentType, (paymentType) => paymentType.sale)
  paymentType: PaymentType;

  @ManyToOne(() => Business, (business) => business.investments)
  @JoinColumn({ name: 'sales_business_id' })
  business: Business;

  @OneToMany(() => SaleDetail, (saleDetail) => saleDetail.sale, {
    cascade: true,
  })
  saleDetails: SaleDetail[];

  @CreateDateColumn({ name: 'sales_created_date' })
  created: Date;

  @UpdateDateColumn({ name: 'sales_updated_date' })
  updated: Date;

  @DeleteDateColumn({ name: 'sales_deleted_date' })
  deleted: Date;
}
