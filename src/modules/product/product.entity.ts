import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Business } from '../business/business.entity';
import { ProductType } from '../product-type/product-type.entity';
import { SaleDetail } from '../sale-detail/sale-detail.entity';

@Entity('products', { schema: 'jaba' })
export class Product {
  @PrimaryGeneratedColumn({ name: 'product_id' })
  id: number;

  @Column({
    type: 'character varying',
    length: '50',
    name: 'product_name',
    nullable: false,
  })
  name: string;

  @Column({
    type: 'character varying',
    length: '50',
    name: 'product_code',
    nullable: false,
  })
  code: string;

  @Column({
    type: 'character varying',
    name: 'product_description',
    length: '255',
    nullable: true,
  })
  description: string;

  @Column({
    type: 'bigint',
    name: 'product_initial_quantity',
    nullable: false,
  })
  initialQuantity: number;

  @Column({
    type: 'bigint',
    name: 'product_quantity',
    nullable: false,
  })
  quantity: number;

  @ManyToOne(() => ProductType, (productType) => productType.product)
  @JoinColumn({ name: 'product_product_type_id' })
  productType: ProductType;

  @ManyToOne(() => Business, (business) => business.products)
  @JoinColumn({ name: 'product_business_id' })
  business: Business;

  @OneToMany(() => SaleDetail, (saleDetail) => saleDetail.product)
  saleDetails: SaleDetail[];

  @CreateDateColumn({ name: 'product_created_date' })
  created: Date;

  @UpdateDateColumn({ name: 'product_updated_date' })
  updated: Date;

  @DeleteDateColumn({ name: 'product_deleted_date' })
  deleted: Date;
}
