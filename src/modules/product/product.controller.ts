import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseIntPipe,
  Post,
  UseFilters,
} from '@nestjs/common';
import { EntityNotFoundErrorFilter } from 'src/common/exceptions/entity-not-found-error.filter';
import { UpdateResult } from 'typeorm';
import { ProductDTO } from './product.dto';
import { Product } from './product.entity';
import { ProductService } from './product.service';

@Controller('product')
export class ProductController {
  constructor(private productService: ProductService) {}

  @Get('/all')
  public findAll(): Promise<Product[]> {
    return this.productService.findAll();
  }

  @Get('/:id')
  @UseFilters(EntityNotFoundErrorFilter)
  public findOne(
    @Param('id', ParseIntPipe) paymentType: number,
  ): Promise<Product> {
    return this.productService.findOne(paymentType);
  }

  @Delete('/:id')
  public softDelete(
    @Param('id', ParseIntPipe) paymentType: number,
  ): Promise<UpdateResult> {
    return this.productService.softDelete(paymentType);
  }

  @Post('/')
  public save(@Body() vendor: ProductDTO): Promise<Product> {
    return this.productService.save(vendor);
  }
}
