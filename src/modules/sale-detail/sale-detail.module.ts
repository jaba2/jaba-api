import { Module } from '@nestjs/common';
import { SaleDetailService } from './sale-detail.service';
import { SaleDetailController } from './sale-detail.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SaleDetail } from './sale-detail.entity';

@Module({
  imports: [TypeOrmModule.forFeature([SaleDetail])],
  providers: [SaleDetailService],
  controllers: [SaleDetailController],
})
export class SaleDetailModule {}
