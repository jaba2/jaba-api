import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Investment } from '../investment/investment.entity';
import { Product } from '../product/product.entity';
import { Sale } from '../sale/sale.entity';
import { Vendor } from '../vendor/vendor.entity';

@Entity('business', { schema: 'jaba' })
export class Business {
  @PrimaryGeneratedColumn({ name: 'business_id' })
  id: number;

  @Column({
    type: 'character varying',
    length: '50',
    name: 'business_name',
    nullable: false,
  })
  name: string;

  @Column({
    type: 'character varying',
    name: 'business_description',
    length: '255',
    nullable: true,
  })
  description: string;

  @Column({
    type: 'bigint',
    name: 'business_investment_amount',
    nullable: false,
  })
  investmentAmount: number;

  @ManyToOne(() => Vendor, (vendor) => vendor.business)
  @JoinColumn({ name: 'business_vendor_id' })
  vendor: Vendor;

  @OneToMany(() => Investment, (investment) => investment.business, {
    cascade: true,
    eager: true,
  })
  investments: Investment[];

  @OneToMany(() => Sale, (sale) => sale.business)
  sales: Sale[];

  @OneToMany(() => Product, (product) => product.business, {
    cascade: true,
    eager: true,
  })
  products: Product[];

  @CreateDateColumn({ name: 'business_created_date' })
  created: Date;

  @UpdateDateColumn({ name: 'business_updated_date' })
  updated: Date;

  @DeleteDateColumn({ name: 'business_deleted_date' })
  deleted: Date;
}
