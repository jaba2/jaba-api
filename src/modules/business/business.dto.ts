import { Type } from 'class-transformer';
import { IsArray, IsNotEmpty, IsString, ValidateNested } from 'class-validator';
import { InvestmentDTO } from '../investment/investment.dto';
import { ProductDTO } from '../product/product.dto';
import { VendorDTO } from '../vendor/vendor.dto';

export class BusinessDTO {
  @IsString()
  @IsNotEmpty()
  name: string;

  @IsString()
  description: string;

  @IsNotEmpty()
  vendor: VendorDTO;

  @IsArray()
  @ValidateNested()
  @Type(() => InvestmentDTO)
  investments: InvestmentDTO[];

  @IsArray()
  @ValidateNested()
  @Type(() => ProductDTO)
  products: ProductDTO[];
}
