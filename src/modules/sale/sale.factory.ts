import { SaleDetail } from '../sale-detail/sale-detail.entity';
import { SaleDTO } from './sale.dto';
import { Sale } from './sale.entity';

export class SaleFactory {
  /**
   * @description
   * @param saleDTO
   * @returns
   */
  public static buildFromDTO(saleDTO: SaleDTO): Sale {
    const sale: Sale = new Sale();
    sale.description = saleDTO.description;
    sale.clientName = saleDTO.clientName;
    sale.quantity = saleDTO.quantity;
    sale.amount = saleDTO.amount;
    sale.business = saleDTO.business;
    sale.saleDetails = saleDTO.saleDetails as SaleDetail[];
    return sale;
  }
}
