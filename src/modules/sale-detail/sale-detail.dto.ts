import { Type } from 'class-transformer';
import { IsNumber, IsObject, IsString } from 'class-validator';
import { Product } from '../product/product.entity';

export class SaleDetailDTO {
  @IsString()
  description: string;

  @IsNumber()
  quantity: number;

  @IsNumber()
  amount: number;

  @IsNumber()
  totalAmount: number;

  @IsObject()
  @Type(() => Product)
  product: Product;
}
