import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, UpdateResult } from 'typeorm';
import { SaleDetailDTO } from './sale-detail.dto';
import { SaleDetail } from './sale-detail.entity';

@Injectable()
export class SaleDetailService {
  constructor(
    @InjectRepository(SaleDetail)
    private saleDetailRepository: Repository<SaleDetail>,
  ) {}
  /**
   * @description Finds all active saleDetail
   * @returns {Promise<SaleDetail[]>} A list with all active saleDetail
   */
  public async findAll(): Promise<SaleDetail[]> {
    return this.saleDetailRepository.find();
  }

  /**
   * @description Finds one saleDetail by id
   * @param {number} saleDetailId
   * @returns {Promise<SaleDetail}> The found saleDetail, if not return an exception
   */
  public async findOne(saleDetailId: number): Promise<SaleDetail> {
    return this.saleDetailRepository.findOneByOrFail({ id: saleDetailId });
  }

  /**
   * @description Applies an soft delete to record
   * @param {number} saleDetailId
   * @returns {Promise<UpdateResult}
   */
  public async softDelete(saleDetailId: number): Promise<UpdateResult> {
    return this.saleDetailRepository.softDelete({ id: saleDetailId });
  }

  /**
   * @description Saves a saleDetail record
   * @param {SaleDetailDTO} saleDetail
   * @returns {SaleDetail} The saleDetail saved
   */
  public async save(saleDetail: SaleDetailDTO): Promise<SaleDetail> {
    return this.saleDetailRepository.save(saleDetail);
  }
}
