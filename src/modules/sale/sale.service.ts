import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DataSource, Repository, UpdateResult } from 'typeorm';
import { Product } from '../product/product.entity';
import { ProductService } from '../product/product.service';
import { SaleDetailDTO } from '../sale-detail/sale-detail.dto';
import { SaleDTO } from './sale.dto';
import { Sale } from './sale.entity';
import { SaleFactory } from './sale.factory';

@Injectable()
export class SaleService {
  constructor(
    @InjectRepository(Sale)
    private saleRepository: Repository<Sale>,
    private productService: ProductService,
    private dataSource: DataSource,
  ) {}
  /**
   * @description Finds all active sale
   * @returns {Promise<Sale[]>} A list with all active sale
   */
  public async findAll(): Promise<Sale[]> {
    return this.saleRepository.find();
  }

  /**
   * @description Finds one sale by id
   * @param {number} saleId
   * @returns {Promise<Sale}> The found sale, if not return an exception
   */
  public async findOne(saleId: number): Promise<Sale> {
    return this.saleRepository.findOneByOrFail({ id: saleId });
  }

  /**
   * @description Applies an soft delete to record
   * @param {number} saleId
   * @returns {Promise<UpdateResult}
   */
  public async softDelete(saleId: number): Promise<UpdateResult> {
    return this.saleRepository.softDelete({ id: saleId });
  }

  /**
   * @description Saves a sale and details
   * @param {SaleDTO} sale
   * @returns {Sale} The sale saved
   */
  public async save(sale: SaleDTO): Promise<Sale> {
    const products: Product[] = this.getProductsWithUpdatedQuantity(
      sale.saleDetails,
    );
    const saleParsed = SaleFactory.buildFromDTO(sale);
    const queryRunner = this.dataSource.createQueryRunner();
    await queryRunner.connect();
    await queryRunner.startTransaction();
    try {
      const saleSaved = await queryRunner.manager.save<Sale>(saleParsed);
      await queryRunner.manager.save<Product[]>(products);
      await queryRunner.commitTransaction();
      return saleSaved;
    } catch (err) {
      console.error(err);
      await queryRunner.rollbackTransaction();
      throw err;
    } finally {
      await queryRunner.release();
    }
  }

  /**
   * @description Update each quantity product in sale detail
   * @param {SaleDetailDTO} saleDetails All sale details in sale
   * @returns {Product[]} Products updated
   */
  private getProductsWithUpdatedQuantity(
    saleDetails: SaleDetailDTO[],
  ): Product[] {
    const products: Product[] = [];
    for (const saleDetail of saleDetails) {
      const { product, quantity } = saleDetail;
      const productUpdated = this.productService.updateQuantityAfterSale(
        product,
        quantity,
      );
      products.push(productUpdated);
    }

    return products;
  }
}
