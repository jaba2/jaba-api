import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, UpdateResult } from 'typeorm';
import { ProductTypeDTO } from './product-type.dto';
import { ProductType } from './product-type.entity';

@Injectable()
export class ProductTypeService {
  constructor(
    @InjectRepository(ProductType)
    private productTypeRepository: Repository<ProductType>,
  ) {}

  /**
   * @description Finds all active productType
   * @returns {Promise<PaymentType[]>} A list with all active productType
   */
  public async findAll(): Promise<ProductType[]> {
    return this.productTypeRepository.find();
  }

  /**
   * @description Finds one productType by id
   * @param {number} productTypeId
   * @returns {Promise<PaymentType}> The found productType, if not return an exception
   */
  public async findOne(productTypeId: number): Promise<ProductType> {
    return this.productTypeRepository.findOneByOrFail({ id: productTypeId });
  }

  /**
   * @description Applies an soft delete to record
   * @param {number} productTypeId
   * @returns {Promise<UpdateResult}
   */
  public async softDelete(productTypeId: number): Promise<UpdateResult> {
    return this.productTypeRepository.softDelete({ id: productTypeId });
  }

  /**
   * @description Saves a productType record
   * @param {PaymentTypeDTO} productType
   * @returns {PaymentType} The productType saved
   */
  public async save(productType: ProductTypeDTO): Promise<ProductType> {
    return this.productTypeRepository.save(productType);
  }
}
