import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Product } from '../product/product.entity';
import { Sale } from '../sale/sale.entity';

@Entity('sale_details', { schema: 'jaba' })
export class SaleDetail {
  @PrimaryGeneratedColumn({ name: 'sale_details_id' })
  id: number;

  @Column({
    type: 'character varying',
    name: 'sale_details_description',
    length: '255',
    nullable: true,
  })
  description: string;

  @Column({
    type: 'bigint',
    name: 'sale_details_quantity',
    nullable: false,
  })
  quantity: number;

  @Column({
    type: 'bigint',
    name: 'sale_details_amount',
    nullable: false,
  })
  amount: number;

  @Column({
    type: 'bigint',
    name: 'sale_details_total_amount',
    nullable: false,
  })
  totalAmount: number;

  @ManyToOne(() => Sale, (sale) => sale.saleDetails)
  @JoinColumn({ name: 'sale_details_sale_id' })
  sale: Sale;

  @ManyToOne(() => Product, (product) => product.saleDetails)
  @JoinColumn({ name: 'sale_details_product_id' })
  product: Product;

  @CreateDateColumn({ name: 'sale_details_created_date' })
  created: Date;

  @UpdateDateColumn({ name: 'sale_details_updated_date' })
  updated: Date;

  @DeleteDateColumn({ name: 'sale_details_deleted_date' })
  deleted: Date;
}
