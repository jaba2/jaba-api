import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { join } from 'path';
import { ConfigKeys } from 'src/config/config.keys';

@Module({
  imports: [
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: (configService: ConfigService) => ({
        type: 'postgres',
        host: configService.get<string>(ConfigKeys.DB_HOST),
        port: configService.get<number>(ConfigKeys.DB_PORT),
        username: configService.get<string>(ConfigKeys.DB_USER),
        password: configService.get<string>(ConfigKeys.DB_PASSWORD),
        database: configService.get<string>(ConfigKeys.DB_NAME),
        logging: configService.get<boolean>(ConfigKeys.DB_LOGGING),
        entities: [join(__dirname, '**', '*.entity.{ts,js}')],
        migrations: [__dirname + '/migration/*{.ts,.js}'],
        synchronize: false,
        autoLoadEntities: true,
      }),
      inject: [ConfigService],
    }),
  ],
})
export class DbModule {}
