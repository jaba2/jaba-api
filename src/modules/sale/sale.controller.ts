import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseIntPipe,
  Post,
  UseFilters,
} from '@nestjs/common';
import { EntityNotFoundErrorFilter } from 'src/common/exceptions/entity-not-found-error.filter';
import { UpdateResult } from 'typeorm';
import { SaleDTO } from './sale.dto';
import { Sale } from './sale.entity';
import { SaleService } from './sale.service';

@Controller('sale')
export class SaleController {
  constructor(private saleService: SaleService) {}

  @Get('/all')
  public findAll(): Promise<Sale[]> {
    return this.saleService.findAll();
  }

  @Get('/:id')
  @UseFilters(EntityNotFoundErrorFilter)
  public findOne(@Param('id', ParseIntPipe) saleId: number): Promise<Sale> {
    return this.saleService.findOne(saleId);
  }

  @Delete('/:id')
  public softDelete(
    @Param('id', ParseIntPipe) saleId: number,
  ): Promise<UpdateResult> {
    return this.saleService.softDelete(saleId);
  }

  @Post('/')
  public save(@Body() sale: SaleDTO): Promise<Sale> {
    return this.saleService.save(sale);
  }
}
