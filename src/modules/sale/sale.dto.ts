import { Type } from 'class-transformer';
import {
  IsArray,
  IsNotEmpty,
  IsNumber,
  IsObject,
  IsString,
} from 'class-validator';
import { Business } from '../business/business.entity';
import { SaleDetailDTO } from '../sale-detail/sale-detail.dto';

export class SaleDTO {
  @IsString()
  description: string;

  @IsString()
  clientName: string;

  @IsNumber()
  quantity: number;

  @IsNumber()
  amount: number;

  @IsObject()
  @Type(() => Business)
  business: Business;

  @IsArray()
  @IsNotEmpty()
  @Type(() => SaleDetailDTO)
  saleDetails: SaleDetailDTO[];
}
