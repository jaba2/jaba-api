import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, UpdateResult } from 'typeorm';
import { PaymentTypeDTO } from './payment-type-dto';
import { PaymentType } from './payment-type.entity';

@Injectable()
export class PaymentTypeService {
  constructor(
    @InjectRepository(PaymentType)
    private paymentType: Repository<PaymentType>,
  ) {}

  /**
   * @description Finds all active paymentType
   * @returns {Promise<PaymentType[]>} A list with all active paymentType
   */
  public async findAll(): Promise<PaymentType[]> {
    return this.paymentType.find();
  }

  /**
   * @description Finds one paymentType by id
   * @param {number} paymentTypeId
   * @returns {Promise<PaymentType}> The found paymentType, if not return an exception
   */
  public async findOne(paymentTypeId: number): Promise<PaymentType> {
    return this.paymentType.findOneByOrFail({ id: paymentTypeId });
  }

  /**
   * @description Applies an soft delete to record
   * @param {number} paymentTypeId
   * @returns {Promise<UpdateResult}
   */
  public async softDelete(PaymentTypeId: number): Promise<UpdateResult> {
    return this.paymentType.softDelete({ id: PaymentTypeId });
  }

  /**
   * @description Saves a paymentType record
   * @param {PaymentTypeDTO} paymentType
   * @returns {PaymentType} The paymentType saved
   */
  public async save(paymentType: PaymentTypeDTO): Promise<PaymentType> {
    return this.paymentType.save(paymentType);
  }
}
