import { BusinessModule } from './modules/business/business.module';
import { ConfigModule } from '@nestjs/config';
import { InvestmentModule } from './modules/investment/investment.module';
import { Module } from '@nestjs/common';
import { PaymentTypeModule } from './modules/payment-type/payment-type.module';
import { ProductModule } from './modules/product/product.module';
import { ProductTypeModule } from './modules/product-type/product-type.module';
import { SaleDetailModule } from './modules/sale-detail/sale-detail.module';
import { SaleModule } from './modules/sale/sale.module';
import { VendorModule } from './modules/vendor/vendor.module';
import { DbModule } from './db/db.module';

@Module({
  imports: [
    ConfigModule.forRoot(),
    DbModule,
    BusinessModule,
    VendorModule,
    InvestmentModule,
    SaleModule,
    ProductModule,
    ProductTypeModule,
    PaymentTypeModule,
    SaleDetailModule,
  ],
})
export class AppModule {}
