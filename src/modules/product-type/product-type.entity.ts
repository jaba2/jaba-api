import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Product } from '../product/product.entity';

@Entity('product_types', { schema: 'jaba' })
export class ProductType {
  @PrimaryGeneratedColumn({ name: 'product_type_id' })
  id: number;

  @Column({
    type: 'character varying',
    length: '50',
    name: 'product_type_name',
    nullable: false,
  })
  name: string;

  @Column({
    type: 'character varying',
    length: '50',
    name: 'product_type_code',
    nullable: false,
  })
  code: string;

  @Column({
    type: 'character varying',
    name: 'product_type_description',
    length: '255',
    nullable: true,
  })
  description: string;

  @OneToMany(() => Product, (product) => product.productType)
  product: Product;

  @CreateDateColumn({ name: 'product_type_created_date' })
  created: Date;

  @UpdateDateColumn({ name: 'product_type_updated_date' })
  updated: Date;

  @DeleteDateColumn({ name: 'product_type_deleted_date' })
  deleted: Date;
}
