import { Module } from '@nestjs/common';
import { InvestmentService } from './investment.service';
import { InvestmentController } from './investment.controller';
import { Investment } from './investment.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [TypeOrmModule.forFeature([Investment])],
  providers: [InvestmentService],
  controllers: [InvestmentController],
})
export class InvestmentModule {}
