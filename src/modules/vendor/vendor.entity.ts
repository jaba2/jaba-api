import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Business } from '../business/business.entity';

@Entity('vendor', { schema: 'jaba' })
export class Vendor {
  @PrimaryGeneratedColumn({ name: 'vendor_id' })
  id: number;

  @Column({
    type: 'character varying',
    length: '50',
    name: 'vendor_name',
    nullable: false,
  })
  name: string;

  @Column({
    type: 'character varying',
    name: 'vendor_description',
    length: '255',
    nullable: true,
  })
  description: string;

  @OneToMany(() => Business, (business) => business.vendor)
  business: Business;

  @CreateDateColumn({ name: 'vendor_created_date' })
  created: Date;

  @UpdateDateColumn({ name: 'vendor_updated_date' })
  updated: Date;

  @DeleteDateColumn({ name: 'vendor_deleted_date' })
  deleted: Date;
}
