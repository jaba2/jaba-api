export interface IConfig {
  DB_HOST: string;
  DB_LOGGING: boolean;
  DB_NAME: string;
  DB_PASSWORD: string;
  DB_PORT: number;
  DB_TYPE: string;
  DB_USER: string;
  PORT: string;
}
