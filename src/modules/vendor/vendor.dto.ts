import { IsString, IsNotEmpty } from 'class-validator';

export class VendorDTO {
  @IsNotEmpty()
  @IsString()
  name: string;

  @IsString()
  description?: string;
}
