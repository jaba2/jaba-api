import { Module } from '@nestjs/common';
import { SaleService } from './sale.service';
import { SaleController } from './sale.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Sale } from './sale.entity';
import { ProductService } from '../product/product.service';
import { Product } from '../product/product.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Sale, Product])],
  providers: [SaleService, ProductService],
  controllers: [SaleController],
})
export class SaleModule {}
