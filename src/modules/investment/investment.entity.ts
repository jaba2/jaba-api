import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Business } from '../business/business.entity';
import { PaymentType } from '../payment-type/payment-type.entity';

@Entity('investments', { schema: 'jaba' })
export class Investment {
  @PrimaryGeneratedColumn({ name: 'investments_id' })
  id: number;

  @Column({
    type: 'character varying',
    length: '50',
    name: 'investments_name',
    nullable: false,
  })
  name: string;

  @Column({
    type: 'character varying',
    name: 'investments_description',
    length: '255',
    nullable: true,
  })
  description: string;

  @Column({
    type: 'bigint',
    name: 'investments_amount',
    nullable: false,
  })
  amount: number;

  @ManyToOne(() => PaymentType, (paymentType) => paymentType.investment)
  paymentType: PaymentType;

  @ManyToOne(() => Business, (business) => business.investments)
  @JoinColumn({ name: 'investments_business_id' })
  business: Business;

  @CreateDateColumn({ name: 'investments_created_date' })
  created: Date;

  @UpdateDateColumn({ name: 'investments_updated_date' })
  updated: Date;

  @DeleteDateColumn({ name: 'investments_deleted_date' })
  deleted: Date;
}
